package chapter_02.task_05;

class DVDPlayer {
	
	boolean canRecord = false;
	
	void playDVD() {
		System.out.println("DVD playing");
	}
	
	void recordDVD() {
		System.out.println("DVD recording");
	}
}