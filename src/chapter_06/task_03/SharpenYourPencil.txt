METHOD: main()
	INVOKE setUpGame() method to create and initialize DotCom objects
	INVOKE startPlaying() method to start the game
	INVOKE finishGame() method to finish the game
END METHOD