package chapter_05.task_02;

public class SimpleDotComGame {
	
	public static void main(String[] args) {
		int numOfGuesses = 0;
		SimpleDotCom dot = new SimpleDotCom();
		int random = (int) (5 * Math.random());
		int[] locations = {random, random + 1, random + 2};
		dot.setLocationCells(locations);
		boolean isAlive = true;
		while (isAlive == true) {
			String userGuess = getUserInput();
			String result = dot.checkYourself(userGuess);
			numOfGuesses++;
			if (result.equals("kill")) {
				isAlive = false;
				System.out.println("You took " + numOfGuesses + " guesses");
			}
		}
	}
	
	public static String getUserInput() {
		int randomInput = (int) (7 * Math.random());
		return "" + randomInput;
	}
}